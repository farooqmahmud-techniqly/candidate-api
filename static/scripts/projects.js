function loadProjects() {
    var table = $("#projects_table");

    $(table).empty();

    $.get('/api/random/projects/5', function (response) {
        var projects = response.projects;

        for (var i = 0; i < projects.length; i++) {
            var currentItem = projects[i];
            if (i === 0) {
                var heading = $("<tr></tr>");
                for (var key in currentItem) {
                    var newHeading = $("<th></th>");
                    $(newHeading).text(key);
                    $(heading).append(newHeading);
                }
                $(table).append(heading);
            }

            var newRow = $("<tr></tr>");
            for (var prop in currentItem) {
                var newCell = $("<td></td>");

                if (Date.parse(currentItem[prop])) {
                    $(newCell).text((new Date(Date.parse(currentItem[prop]))).toDateString());
                }
                else {
                    $(newCell).text(currentItem[prop].toString());
                }
                $(newRow).append(newCell);
            }
            $(table).append(newRow);
        }
    });
}
