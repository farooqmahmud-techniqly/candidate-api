from flask import jsonify
from flask import render_template


class WebSiteConfig():
    def __init__(self, app, data_provider):
        self.__app = app
        self.__data_provider = data_provider

    def init_website_routes(self):
        if self.__app:
            self.__app.add_url_rule("/about", "page_about", self.__page_about, methods=["GET"])
            self.__app.add_url_rule("/project", "page_project", self.__page_project, methods=["GET"])
            self.__app.add_url_rule("/candidate", "page_candidate", self.__page_candidate, methods=["GET"])
            self.__app.add_url_rule("/experience", "page_experience", self.__page_experience, methods=["GET"])
            self.__app.add_url_rule("/", "page_index", self.__page_index, methods=["GET"])

    @staticmethod
    def __page_index():
        return render_template("index.html", selected_menu_item="index")

    @staticmethod
    def __page_project():
        return render_template("project.html", selected_menu_item="project")

    def __page_candidate(self):
        # candidates = get_candidates()
        candidates = self.__data_provider.get_candidates()
        return render_template("candidate.html", selected_menu_item="candidate", candidates=candidates)

    @staticmethod
    def __page_experience():
        return render_template("experience.html", selected_menu_item="experience")

    @staticmethod
    def __page_about():
        return render_template("about.html", selected_menu_item="about")

    def __random_projects(self, count):
        projects = self.__data_provider.get_random_projects(count)
        return jsonify({"projects": projects, "total": len(projects)})
