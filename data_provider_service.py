from candidate import Candidate
from experience import Experience
from project import Project
from data_generator import DataGenerator

import datetime


class DataProviderService:
    def __init__(self, count):
        self.data_generator = DataGenerator()
        self.CANDIDATES = self.data_generator.generate_candidates(15)

    def get_candidate(self, id):
        result = None

        if id:
            for c in self.CANDIDATES:
                if id == str(c["id"]):
                    result = c
                    break

        return result

    def get_candidates(self):
        return self.CANDIDATES

    def get_random_candidates(self, count):
        return self.data_generator.generate_candidates(count)

    def get_random_projects(self, count):
        return self.data_generator.generate_projects(count, True)

    def add_candidate(self, first_name, last_name):
        candidate = Candidate(first_name, last_name)
        self.CANDIDATES.append(candidate.serialize())
        return str(candidate.id)

    def update_name(self, id, new_first_name, new_last_name):
        updated_count = 0

        for c in self.CANDIDATES:
            if id == str(c["id"]):
                c["first_name"] = new_first_name
                c["last_name"] = new_last_name
                updated_count += 1
                break

        return updated_count

    def delete_candidate(self, id):
        deleted_candidate = None

        for c in self.CANDIDATES:
            if id == str(c["id"]):
                deleted_candidate = c
                break

        if deleted_candidate is not None:
            self.CANDIDATES.remove(deleted_candidate)
            return True
        else:
            return False
