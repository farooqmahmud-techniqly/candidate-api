from flask import jsonify
from flask import abort
from flask import make_response
from flask import request
from flask import url_for


class ApiConfig():
    def __init__(self, app, data_provider):
        self.__app = app
        self.__data_provider = data_provider

    def __list_routes(self):
        result = []
        for r in self.__app.url_map.iter_rules():
            result.append({
                "methods": list(r.methods),
                "route": str(r)
            })

        return jsonify({"routes": result, "count": len(result)})

    def __get_candidates(self):
        candidates = self.__data_provider.__get_candidates()
        return jsonify({"candidates": candidates, "count": len(candidates)})

    def __candidate_by_id(self, id):
        candidate = self.__data_provider.get_candidate(id)

        if candidate:
            return jsonify({"candidate": candidate})
        else:
            abort(404)

    def __update_name(self, id, new_first_name, new_last_name):
        updated_count = self.__data_provider.__update_name(id, new_first_name, new_last_name)

        if updated_count == 0:
            abort(404)
        else:
            return jsonify({"updated_count": updated_count})

    def __random_candidates(self, count):
        candidates = self.__data_provider.get_random_candidates(count)
        return jsonify({"candidates": candidates, "count": len(candidates)})

    def __random_projects(self, count):
        projects = self.__data_provider.get_random_projects(count)
        return jsonify({"projects": projects, "count": len(projects)})

    def __delete(self, id):
        if self.__data_provider.delete_candidate(id):
            return make_response("", 204)
        else:
            return abort(404)

    def __add_candidate(self):
        first_name = request.form["first_name"]
        last_name = request.form["last_name"]

        id = self.__data_provider.__add_candidate(first_name, last_name)

        return jsonify({
            "id": id,
            "url": url_for("candidate_by_id", id=id)
        })

    def init_api_routes(self):
        self.__app.add_url_rule("/api", "list_routes", self.__list_routes)

        self.__app.add_url_rule("/api/candidates", "candidates", self.__get_candidates)

        self.__app.add_url_rule("/api/candidates/<string:id>", "candidate_by_id", self.__candidate_by_id)

        self.__app.add_url_rule(
            "/api/candidates/<string:id>/firstName/<string:new_first_name>/lastName/<string:new_last_name>",
            "update_name", self.__update_name, methods=["PUT"])

        self.__app.add_url_rule("/api/random/candidates/<int:count>", "random_candidates", self.__random_candidates)

        self.__app.add_url_rule("/api/random/projects/<int:count>", "random_projects", self.__random_projects)

        self.__app.add_url_rule("/api/candidates/<string:id>", "delete", self.__delete, methods=["DELETE"])

        self.__app.add_url_rule("/api/candidates", "add_candidate", self.__add_candidate, methods=["POST"])
