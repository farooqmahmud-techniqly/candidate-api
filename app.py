from flask import Flask
from api_config import ApiConfig
from data_provider_service import DataProviderService
from web_site_config import WebSiteConfig

DATA_PROVIDER = DataProviderService(15)

app = Flask(__name__)


@app.template_filter("senior_candidate")
def senior_candidate(candidates):
    result = []
    for c in candidates:
        for exp in c["experience"]:
            if exp["years"] >= 5:
                result.append({
                    "first_name": c["first_name"],
                    "last_name": c["last_name"],
                    "years": exp["years"],
                    "domain": exp["domain"]
                })
                break
    return result

apiConfig = ApiConfig(app, DATA_PROVIDER)
apiConfig.init_api_routes()

webSiteConfig = WebSiteConfig(app, DATA_PROVIDER)
webSiteConfig.init_website_routes()

if __name__ == "__main__":
    app.run(debug=True)
